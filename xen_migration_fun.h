static void save_domain_core_begin(uint32_t domid,
                                   const char *override_config_file,
                                   uint8_t **config_data_r,
                                   int *config_len_r)


static void save_domain_core_writeconfig(int fd, const char *source,
                                  const uint8_t *config_data, int config_len)


static int save_domain(uint32_t domid, const char *filename, int checkpoint,
                            int leavepaused, const char *override_config_file)

static pid_t create_migration_child(const char *rune, int *send_fd,
                                        int *recv_fd)


static int migrate_read_fixedmessage(int fd, const void *msg, int msgsz,
                                     const char *what, const char *rune) {

static void migration_child_report(int recv_fd )

static void migrate_do_preamble(int send_fd, int recv_fd, pid_t child,
                                uint8_t *config_data, int config_len,
                                const char *rune)

static void migrate_domain(uint32_t domid, const char *rune, int debug,
                           const char *override_config_file)

static void migrate_receive(int debug, int daemonize, int monitor,
                            int pause_after_migration,
                            int send_fd, int recv_fd,
                            libxl_checkpointed_stream checkpointed,
                            char *colo_proxy_script)

int main_restore(int argc, char **argv)

int main_migrate_receive(int argc, char **argv)

int main_save(int argc, char **argv)

int main_migrate(int argc, char **argv)
